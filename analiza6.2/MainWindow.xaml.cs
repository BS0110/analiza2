﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForSchoolWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> listStrings = new List<string>();
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\data.txt";
        string rijec;

        int strikes = 5;

        public MainWindow()
        {
            InitializeComponent();
            Load();
        }

        void Load()
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    listStrings.Add(line);
                }
            }
            System.Random rand = new System.Random();
            if (listStrings.Count <= 0)
            {
                return;
            }
            int pojam = rand.Next(1, listStrings.Count + 1);
            string l = listStrings[pojam - 1];
            Field.Text = "";
            foreach (char c in l)
            {
                Field.Text += "_ ";
            }
            rijec = l;
        }

        private void In_Click(object sender, RoutedEventArgs e)
        {
            if (Field.Text == "Bravo! Za novu igru kliknite gumb unesi!")
            {
                Field.Text = "";
                strikes = 5;
                TriesLeft.Text = "Preostalo pokušaja: " + strikes.ToString();
                LettersTried.Text = "";
                Load();
                return;
            }

            if (LetterIn.Text.Length != 1)
            {
                return;
            }
            List<int> positions = new List<int>();
            int i = -1;
            char input = LetterIn.Text.ToCharArray()[0];
            foreach (char let in LettersTried.Text)
            {
                if (let == input)
                {
                    return;
                }
            }
            foreach (char c in rijec)
            {
                i++;
                if (c == input)
                {
                    positions.Add(i);
                }
            }
            string currsol = "";
            if (positions.Count == 0)
            {
                strikes--;
                TriesLeft.Text = "Preostalo pokušaja: " + strikes.ToString();
                LettersTried.Text += input + " ";
                return;
            }
            else
            {
                for (int j = 0; j < Field.Text.Length; j += 2)
                {
                    bool flagg = true;
                    foreach (int z in positions)
                    {
                        if (j / 2 == z)
                        {
                            currsol += rijec[j / 2];
                            flagg = false;
                        }
                    }
                    if (flagg)
                    {
                        currsol += Field.Text.ToCharArray()[j];
                    }
                    currsol += " ";
                }
            }
            Field.Text = currsol;
            LettersTried.Text += input + " ";

            bool flaggie2 = true;
            foreach (char character in Field.Text)
            {
                if (character == '_')
                {
                    flaggie2 = false;
                }
            }

            if (flaggie2)
            {
                Field.Text = "Bravo! Za novu igru kliknite gumb unesi!";
            }
        }
    }
}